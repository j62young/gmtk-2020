﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RodExposurePercent : MonoBehaviour
{
    public Rod sprite1;
    public Rod sprite2;
    public Rod sprite3;
    public Rod sprite4;

    private RodEjectScript rod1;
    private RodEjectScript rod2;
    private RodEjectScript rod3;
    private RodEjectScript rod4;

    private float rodExposureSum = 0;
    private float maxExposureSum;
    public float sicknessRatePercent;
    private float updateSpeed = 0.25f; //in seconds to avoid lots of calculation like in Update(). 

    // Start is called before the first frame update
    void Start()
    {

        rod1 = sprite1.GetComponent<RodEjectScript>();
        rod2 = sprite2.GetComponent<RodEjectScript>();
        rod3 = sprite3.GetComponent<RodEjectScript>();
        rod4 = sprite4.GetComponent<RodEjectScript>();

        maxExposureSum = rod1.rodTranslateDistance * 4;
        sicknessRatePercent = 0.0f;

        updateRodExposureSum();
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Timer is updated by 1 every second. 
    private void updateRodExposureSum()
    {
        rodExposureSum = rod1.getRodExposure() + rod2.getRodExposure() + rod3.getRodExposure() + rod4.getRodExposure();

        sicknessRatePercent = rodExposureSum / maxExposureSum;
        Invoke("updateRodExposureSum", updateSpeed);
    }

    public float getsicknessRatePercent ()
    {
        return sicknessRatePercent;
    }
}
