﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RodTimerScript : MonoBehaviour
{
    public int movementStartTime;
    private int currentTime = 0;
    private RodEjectScript rodEjectScript;

    // Start is called before the first frame update
    void Start()
    {
        rodEjectScript = GetComponent<RodEjectScript>();
        //GameObject.GetComponent<RodEjectScript>().enabled = true;
        updateTimer();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Timer is updated by 1 every second. 
    private void updateTimer()
    {
        if(currentTime == movementStartTime)
        {
            rodEjectScript.enabled = true;
        }

        currentTime += 1;
        Invoke("updateTimer", 1f);
    }
}

