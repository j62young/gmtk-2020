﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RodEjectScript : MonoBehaviour
{

    public float speed = 0.5f;
    private float lowerDistanceLimit;
    public float rodTranslateDistance = 4f;
    private float rodExposure = 0; //can go up to max of rodTranslateDistance
    private Vector3 initialPos;

    // Start is called before the first frame update
    void Start()
    {
        lowerDistanceLimit = transform.position.y - rodTranslateDistance;
        initialPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        //Variable to hold current position
        Vector3 pos = transform.position;

        //Lower distance limit rod can travel out of the reactor. 
        if (pos.y >= lowerDistanceLimit)
       {
            //Rod steadily is moving in the y direction (down the screen) aka "ejecting" out of the nuclear reactor. 
            transform.Translate(Vector3.down * Time.deltaTime);
            
        }

        //Update rodExposure Percentage
        rodExposure = initialPos.y - pos.y;
    }

    public float getRodExposure ()
    {
        return rodExposure;
    }
}
