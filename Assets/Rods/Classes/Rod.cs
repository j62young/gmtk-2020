﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rod : InteractableObject
{
    [Header("Rod Movement")]

    [SerializeField]
    private float speed = 0.5f;

    [SerializeField]
    private float rodTranslateDistance = 4.0f;

    [SerializeField]
    private float lowerDistanceLimit = 0.0f;

    private void Awake()
    {
        lowerDistanceLimit = transform.position.y - rodTranslateDistance;
    }

    protected override void Start()
    {
        base.Start();
    }

    private void Update()
    {
        //RodEjection();
    }

    private void RodEjection()
    {
        //Variable to hold current position
        Vector3 pos = transform.position;

        //Lower distance limit rod can travel out of the reactor. 
        if (pos.y >= lowerDistanceLimit)
        {
            //Rod steadily is moving in the y direction (down the screen) aka "ejecting" out of the nuclear reactor. 
            transform.Translate(Vector3.down * Time.deltaTime);
        }
    }

    public override void Interact()
    {
        base.Interact();
    }
}
