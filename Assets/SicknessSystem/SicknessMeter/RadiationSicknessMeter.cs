﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadiationSicknessMeter : MonoBehaviour
{
    /// <summary>
    /// A reference to the CharacterSickness scriptable object that keeps our player's radiation sickness value
    /// </summary>
    [SerializeField]
    private CharacterSickness playerSickness = default;

    [Header("References")]

    [SerializeField]
    private Image sicknessMeterBar = default;

    [SerializeField]
    private Image sicknessPortrait = default;

    private void Awake()
    {
        //Hook in to all our radiation sickness value events
        playerSickness.RadiationSicknessValue.ValueChangedEvent.AddListener(UpdateSicknessMeter);
        playerSickness.RadiationSicknessValue.ValueAtMaxEvent.AddListener(OnMaxRadiation);
        playerSickness.RadiationSicknessValue.ValueAtMinEvent.AddListener(OnMinRadiation);
        playerSickness.RadiationSicknessValue.InitComponent();
    }

    public void UpdateSicknessMeter(float inValue)
    {
        float meterFill = inValue / playerSickness.RadiationSicknessValue.MaxValue;

        sicknessMeterBar.fillAmount = meterFill;
    }

    public void OnMaxRadiation()
    {
        //Nothing yet
    }

    public void OnMinRadiation()
    {
        //Nothing yet
    }
}
