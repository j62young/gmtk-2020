﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ComponentSystem;

[CreateAssetMenu(fileName = "CharacterSickness", menuName = "RadiationSicknessSystem/CharacterSickness")]
public class CharacterSickness : ScriptableObject
{
    [Header("Default Settings")]

    [SerializeField]
    private float startingSicknessValue = 0.0f;

    [SerializeField]
    private float maxSicknessValue = 100.0f;

    [SerializeField]
    private float minSicknessValue = 0.0f;

    [Header("Components")]

    [SerializeField]
    private ValueComponent radiationSickness = new ValueComponent();

    public ValueComponent RadiationSicknessValue { get { return radiationSickness; } }

    public void AddRadiationSickness(float valueToAdd)
    {
        radiationSickness.UpdateValueByValue(valueToAdd);
    }

    public void ResetRadiation()
    {
        radiationSickness = new ValueComponent(startingSicknessValue, maxSicknessValue, minSicknessValue);
    }

    private void OnDisable()
    {
        radiationSickness = new ValueComponent(startingSicknessValue, maxSicknessValue, minSicknessValue);
    }
}
