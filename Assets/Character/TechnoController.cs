﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TechnoController : MonoBehaviour
{
    [Header("Stats")]
    /// <summary>
    /// A reference to the CharacterSickness scriptable object that keeps our player's radiation sickness value
    /// </summary>
    [SerializeField]
    private CharacterSickness playerSickness = default;
    private enum State {
        normal,
        sick,
        crawling,
        dead
    }
    private State TechnoState;
    [SerializeField]
    private float speed = 10;
    [SerializeField]
    private Camera characterCamera;
    private Vector2 forwardVector;

    [SerializeField] [ReadOnly]
    private InteractableObject objectInteractingWith = null;
    private bool isPuking;
    private float vomitTimer;
    public float vomitWait = 3.0f;
    public float vomitDurationTimer;
    public float vomitDuration = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        if (characterCamera == null)
            characterCamera = Camera.main;

        TechnoState = State.normal;
        playerSickness.RadiationSicknessValue.ValueChangedEvent.AddListener(UpdateState);
        vomitTimer = 0.0f;
        vomitDurationTimer = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (vomitTimer > vomitWait) {
            vomitDurationTimer += Time.deltaTime;
            if (vomitDurationTimer < vomitDuration) {
                // vomiting animation will play during this interim
                return;
            }

            vomitTimer = 0.0f;
            vomitDurationTimer = 0.0f;
        }

        switch (TechnoState)
        {
            case State.dead:
                OnKilled();
                break;
            case State.sick:
                vomitTimer += Time.deltaTime;
                CalculateForwardVector();
                MoveCharacter();
                break;
            default:
                CalculateForwardVector();
                MoveCharacter();
                break;
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            InteractWithObject();
        }
    }

    public void UpdateState(float inValue)
    {
        if (inValue > 25.0f && inValue < 75.0f)
        {
            TechnoState = State.sick;
        }
        else if (inValue > 75.0f && inValue < 100.0f)
        {
            TechnoState = State.crawling;
        } else if (inValue >= 100.0f) {
            TechnoState = State.dead;
        }
    }

    private void CalculateForwardVector()
    {
        forwardVector.Set(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    }

    //Added to pass to animation script
    public Vector2 getForwardVector()
    {
        return forwardVector;
    }

    private void MoveCharacter()
    {
        switch (TechnoState) {
            case State.normal:
                this.transform.Translate(forwardVector * speed * Time.deltaTime, Space.Self);
                break;
            case State.sick:
                this.transform.Translate(forwardVector * speed * Random.Range(0.01f, 1.0f) * Time.deltaTime, Space.Self);
                break;
            case State.crawling:
                this.transform.Translate(forwardVector * speed * 0.3f * Time.deltaTime, Space.Self);
                break;
            default:
                break;
        }

        characterCamera.transform.position = new Vector3(transform.position.x, transform.position.y, characterCamera.transform.position.z);
    }

    public void puke()
    {
        Debug.Log("puking");
        isPuking = false;
    }

    public void takeDamage(float damageTaken)
    {
        // may handle animation update and  communicating with radiation state
    }

    public void OnKilled()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }

    private void InteractWithObject()
    {
        if (objectInteractingWith)
        {
            objectInteractingWith.Interact();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<InteractableObject>())
        {
            objectInteractingWith = collision.gameObject.GetComponent<InteractableObject>();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<InteractableObject>())
        {
            if (collision.gameObject.GetComponent<InteractableObject>() == objectInteractingWith)
            {
                objectInteractingWith = null;
            }
        }
    }
}