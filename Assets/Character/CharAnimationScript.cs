﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAnimationScript : MonoBehaviour
{
    //Reference to animator component
    public Animator animator;

    public SpriteRenderer sprite;

    //Reference to controller script
    public TechnoController controllerScript;
    private Vector2 forwardVector;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        forwardVector = controllerScript.getForwardVector();

        Debug.Log(forwardVector);

        if(forwardVector.Equals(Vector2.up) )
        {
            animator.SetBool("MovingUp", true);
            sprite.flipX = false;

            animator.SetBool("Idle", false);
            animator.SetBool("MovingDown", false);
            animator.SetBool("MovingLeft", false);
            animator.SetBool("MovingRight", false);

            
        }

        else if (forwardVector.Equals(Vector2.down))
        {
            animator.SetBool("MovingDown", true);
            sprite.flipX = false;

            animator.SetBool("Idle", false);
            animator.SetBool("MovingUp", false);
            animator.SetBool("MovingLeft", false);
            animator.SetBool("MovingRight", false);

        }

        else if (forwardVector.Equals(Vector2.left))
        {
            animator.SetBool("MovingLeft", true);
            sprite.flipX = true;

            animator.SetBool("Idle", false);
            animator.SetBool("MovingUp", false);
            animator.SetBool("MovingDown", false);
            animator.SetBool("MovingRight", false);

        }

        else if (forwardVector.Equals(Vector2.right))
        {
            animator.SetBool("MovingRight", true);
            sprite.flipX = false;

            animator.SetBool("Idle", false);
            animator.SetBool("MovingUp", false);
            animator.SetBool("MovingDown", false);
            animator.SetBool("MovingLeft", false);

           }

        else if (forwardVector.Equals(Vector2.zero))
        {
            animator.SetBool("Idle", true);
            sprite.flipX = false;

            animator.SetBool("MovingUp", false);
            animator.SetBool("MovingDown", false);
            animator.SetBool("MovingLeft", false);
            animator.SetBool("MovingRight", false);

        }

    }
}
