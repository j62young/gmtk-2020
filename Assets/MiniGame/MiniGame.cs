﻿using ComponentSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MiniGame : MonoBehaviour
{
    [Header("Winning Bracket")]

    [SerializeField]
    private float topWinningBracket = 0.0f;

    [SerializeField]
    private float bottomWinningBracket = 0.0f;

    [Header("References")]

    [SerializeField]
    private Slider ball = default;

    [SerializeField]
    private GameObject miniGameObject = default;

    [SerializeField][ReadOnly]
    private InteractableObject interactingObject = null;

    [Header("Movement")]

    [SerializeField]
    private float movementRate = 0.1f;

    [SerializeField][ReadOnly]
    private float calculatedMovement = 0.0f;

    [SerializeField] [ReadOnly]
    private BallState ballMovement = BallState.Increase;

    [SerializeField][ReadOnly]
    private bool isStopped = false;

    [Header("Events")]

    [SerializeField]
    private UnityEvent PassedMiniGameEvent = default;

    [SerializeField]
    private UnityEvent FailedMiniGameEvent = default;

    private void Awake()
    {
        miniGameObject.SetActive(false);
    }

    private void Update()
    {
        MoveBall();
    }

    private void MoveBall()
    {
        if (!isStopped)
        {
            CalculateMovement();

            ball.value += calculatedMovement;

            if ((ball.value >= ball.maxValue && ballMovement == BallState.Increase) || (ball.value <= ball.minValue && ballMovement == BallState.Decrease))
            {
                ToggleMovementState();
            }
        }
    }

    private void CalculateMovement()
    {
        switch (ballMovement)
        {
            case BallState.Increase:
                calculatedMovement = movementRate * Time.deltaTime;
                break;

            case BallState.Decrease:
                calculatedMovement = movementRate * -1.0f * Time.deltaTime;
                break;

            default:
                throw new System.Exception("MiniGame.cs ballMovement is using an unsupported state.");
        }      
    }

    public void SetNewMovementRate(float inRate)
    {
        movementRate = inRate;
    }

    public void ToggleMovementState()
    {
        if(ballMovement == BallState.Increase)
        {
            ballMovement = BallState.Decrease;
        }
        else
        {
            ballMovement = BallState.Increase;
        }
    }

    public void StopTheBall()
    {
        isStopped = true;
        float stoppedValue = ball.value;

        Debug.Log(stoppedValue);

        if(stoppedValue >= bottomWinningBracket && stoppedValue <= topWinningBracket)
        {
            PassedMiniGameEvent.Invoke();
        }
        else
        {
            FailedMiniGameEvent.Invoke();
        }

        interactingObject.ResetObject();
        Deactivate();
    }

    public void Activate(InteractableObject inObject)
    {
        interactingObject = inObject;
        isStopped = false;
        miniGameObject.SetActive(true);
        ball.value = ball.minValue;
        ballMovement = BallState.Increase;
    }

    public void Deactivate()
    {
        miniGameObject.SetActive(false);
        interactingObject = null;
    }
}

public enum BallState
{
    Increase,
    Decrease
}
