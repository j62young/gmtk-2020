﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[System.Serializable]
public class FloatValueEvent : UnityEvent<float>
{
    //We don't need anything in here.  We derive this class from UnityEvent and make it serializable so that it will show up in the inspector
    //Custom UnityEvents will not show up in the inspector by default (meaning if you want it to show up in the inspector but your event has a parameter you have to do this to make that happen)
}
