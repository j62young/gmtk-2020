﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

//Namespace makes sure only classes that have "using ComponentSystem" will be able to utilize this component
namespace ComponentSystem
{
    /// <summary>
    /// Value Component is a generic component to handle a float value
    /// System.Serializable allows this standard C# class to be shown in the inspector
    /// </summary>
    [System.Serializable]
    public class ValueComponent
    {

        [Header("Values")]

        /// <summary>
        /// The value of the component
        /// </summary>
        [SerializeField][ReadOnly]
        private float componentValue = 0.0f;

        /// <summary>
        /// The maximum value the component can reach
        /// </summary>
        [SerializeField][ReadOnly]
        private float maxValue = 100.0f;

        /// <summary>
        /// The minimum value the component can reach
        /// </summary>
        [SerializeField][ReadOnly]
        private float minValue = 0.0f;

        [Header("Details")]

        /// <summary>
        /// A string for the name of the value
        /// This value should only be set in the inspector
        /// </summary>
        [SerializeField]
        private string valueName = "";

        /// <summary>
        /// A string for the description of the value
        /// TextArea expands the area to write text in the inspector
        /// This variable should only be set in the inspector
        /// </summary>
        [SerializeField][TextArea(3, 5)]
        private string valueDescription = "";

        /// <summary>
        /// An event that is called when the value is changed/updated
        /// Initialized to default which is similar to null
        /// </summary>
        private FloatValueEvent valueChangedEvent = default;

        /// <summary>
        /// An event that is called when the value is at max
        /// </summary>
        private UnityEvent valueAtMaxEvent = default;

        /// <summary>
        /// An event that is called when the value is at min
        /// </summary>
        private UnityEvent valueAtMinEvent = default;

        #region Constructors
        /// <summary>
        /// Basic constructor
        /// </summary>
        public ValueComponent()
        {
            Value = 0.0f;
            MaxValue = 100.0f;
            MinValue = 0.0f;
        }

        /// <summary>
        /// Constructor for setting the component with a specific value
        /// </summary>
        /// <param name="inValue">The value the initialize the component with</param>
        public ValueComponent(float inValue)
        {
            Value = inValue;
            MaxValue = 100.0f;
            MinValue = 0.0f;
        }

        /// <summary>
        /// Constructor for initializing the component with a specific value, max value, and min value
        /// </summary>
        /// <param name="inValue">The value the component will start with</param>
        /// <param name="inMax">The max value the component will be able to reach</param>
        /// <param name="inMin">The min value the component will be able to reach</param>
        public ValueComponent(float inValue, float inMax, float inMin)
        {
            Value = inValue;
            MaxValue = inMax;
            MinValue = inMin;
        }
        #endregion

        /// <summary>
        /// Function call to initialize the component by invoking the events needed for the start of the component's life
        /// </summary>
        public void InitComponent()
        {
            UpdateValueByValue(0.0f);
        }

        /// <summary>
        /// Update the component value by the input value
        /// Positive numbers will be added to the value
        /// Negative number will be subtracted from the value
        /// </summary>
        /// <param name="inValue">The value to update the component value</param>
        public virtual void UpdateValueByValue(float inValue)
        {
            //Update the value by the input amount
            Value += inValue;
            
            //Check to see if the updated value is over or equal to the max value
            if(Value >= MaxValue)
            {
                //If so, set it to max value so it stays here
                Value = MaxValue;

                //Call our value at max event
                valueAtMaxEvent.Invoke();

            }   //If the value is not equal to or over the max value, check if it's equal to or under the min value
            else if(Value <= MinValue)
            {
                //If so set the value to the min value so it stays there
                Value = MinValue;

                //Call our value at min event
                valueAtMinEvent.Invoke();
            }

            //Call our value changed event so anything that needs to know about it is updated too
            valueChangedEvent.Invoke(Value);
        }

        #region Public Accessors
        /// <summary>
        /// Property for access to the component value
        /// </summary>
        public float Value { get { return componentValue; } protected set { componentValue = value; } }

        /// <summary>
        /// Property for access to the max value
        /// </summary>
        public float MaxValue { get { return maxValue; } protected set { maxValue = value; } }

        /// <summary>
        /// Property for access to the min value
        /// </summary>
        public float MinValue { get { return minValue; } protected set { minValue = value; } }

        /// <summary>
        /// Property for public access to the valueName which should only be getting set in the inspector
        /// </summary>
        public string ValueName { get { return valueName; } }

        /// <summary>
        /// Property for public access to the valueDescription which should only be getting set in the inspector
        /// </summary>
        public string Description { get { return valueDescription; } }

        /// <summary>
        /// Property to publically access the valueChangedEvent
        /// </summary>
        public FloatValueEvent ValueChangedEvent { get { return valueChangedEvent; } }

        /// <summary>
        /// Property to publically access the valueAtMaxEvent
        /// </summary>
        public UnityEvent ValueAtMaxEvent { get { return valueAtMaxEvent; } }

        /// <summary>
        /// Property to publically access the valueAtMinEvent
        /// </summary>
        public UnityEvent ValueAtMinEvent { get { return valueAtMinEvent; } }
        #endregion
    }
}
