﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    [Header("Interactable Object")]

    [SerializeField][ReadOnly]
    private Vector3 originPos = default;

    [SerializeField][ReadOnly]
    private MiniGame miniGameReference = default;

    protected virtual void Start()
    {
        originPos = transform.position;
        miniGameReference = FindObjectOfType<MiniGame>();
    }

    public virtual void Interact()
    {
        if (miniGameReference)
            miniGameReference.Activate(this);
    }

    public void ResetObject()
    {
        transform.position = originPos;
    }
}
